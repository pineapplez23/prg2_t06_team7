﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
//============================================================
// Student Number : S10223312, S10221799
// Student Name : Chee Jia Hau, Denzel Lee Eu Han
// Module Group : P06
//============================================================

namespace Prg2_Assignment
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Movie> MovieList = new();

            List<Cinema> CinemaList = new();

            List<Screening> ScreeningList = new();

            mainmenu(MovieList, CinemaList, ScreeningList);


        }



        // Main menu function//
        static void mainmenu(List<Movie> MovieList, List<Cinema> CinemaList, List<Screening> ScreeningList)
        {

            Console.WriteLine("Welcome to Singa Cineplexes!");

            Console.WriteLine("=======================================");

            Console.WriteLine("[1] Load Movies and Cinema Data");

            Console.WriteLine("[2] Load Screening Data");

            Console.WriteLine("[3] Display Movies ");

            Console.WriteLine("[4] List Movie Screenings");

            Console.WriteLine("[5] Add Movie Screening Session");

            Console.WriteLine("[6] Delete Movie Screening Session");

            Console.WriteLine("[7] Order Movie Tickets");

            Console.WriteLine("[8] Cancel Order of Movie Ticket");

            Console.WriteLine("========================================");

            while (true)
            {
                try
                {
                    int selection;
                    Console.WriteLine("Please make a selection(between 1 to 8): ");
                    selection = Convert.ToInt16(Console.ReadLine());
                    while (true)
                    {
                        if (selection > 8 || selection < 1)
                        {
                            Console.WriteLine("Please choose a proper number: ");
                            Console.WriteLine(" ");
                            Console.WriteLine("Please make a selection(between 1 to 8): ");
                            selection = Convert.ToInt16(Console.ReadLine());
                        }
                        else
                        {
                            if (selection == 1)
                            {
                                Movie(MovieList);
                                Cinema(CinemaList);
                            }
                            if (selection == 2)
                            {
                                Screening(ScreeningList, MovieList, CinemaList);
                            }
                            if (selection == 3)
                            {
                                DisplayMovie(MovieList);
                            }
                            if (selection == 4)
                            {
                                ListMovieScreening();

                            }
                            if (selection == 5)
                            {
                                AddMovieSession(MovieList, CinemaList, ScreeningList);
                            }

                            if (selection == 6)
                            {
                                DeleteMovieScreening(ScreeningList, MovieList);
                            }

                            if (selection == 7)
                            {
                                Console.WriteLine("Incomplete");
                            }

                            if (selection == 8)
                            {
                                Console.WriteLine("Incomplete");
                            }
                            break;
                        }
                    }
                    break;
                }

                catch (Exception ex)
                {
                    if (ex is FormatException)
                    {
                        Console.WriteLine(ex.Message);
                        Console.WriteLine("Invalid");
                    }

                }
            }
        }


        // function that loads movies//
        static void Movie(List<Movie> MovieList)
        {
            using StreamReader sr = new("Movie.csv");
            string s = sr.ReadLine();
            List<string> GenreList = new();

            while ((s = sr.ReadLine()) != null)
            {
                string[] x = s.Split(",");
                string Title = x[0];
                int Duration = Convert.ToInt32(x[1]);
                string genre = x[2];
                string Classification = (x[3]);
                DateTime OpeningDate = Convert.ToDateTime(x[4]);
                GenreList.Add(genre);

                Movie m = new(Title, Duration, Classification, OpeningDate, GenreList);
                MovieList.Add(m);
            }
        }

        //Displays Movie
        static void DisplayMovie(List<Movie> MovieList)
        {
            Console.WriteLine("{0, -20} {1, 10} {2, 10} {3, 10} {4, 10}", "Title", "Duration", "Classification", "Opening Date", "Genre");
            for (int i = 0; i < MovieList.Count; i++)
            {
                Console.WriteLine(MovieList[i].Title, MovieList[i].Duration, MovieList[i].Classification, MovieList[i].OpeningDate, MovieList[i].GenreList[i]);
            }
        }

        //Dont delete
        //for (int i = 0 ; i < MovieList.count(); i++)
        //{
        //   Console.WriteLine(MovieList[i].title, MovieList[i], .....);
        //}


        //Function that loads cinemas//
        static void Cinema(List<Cinema> CinemaList)
        {
            string[] Cinema = File.ReadAllLines("Cinema.csv");

            for (int i = 1; i < Cinema.Length; i++)
            {
                string[] x = Cinema[i].Split(",");
                string Name = x[0];
                int HallNo = Convert.ToInt32(x[1]);
                int Capacity = Convert.ToInt32(x[2]);

                Cinema c = new(Name, HallNo, Capacity);
                CinemaList.Add(c);
            }
        }

        //Function that Displays Cinema//
        static void DisplayCinema(List<Cinema> CinemaList)
        {
            Console.WriteLine("{0, 10} {1, 10} {2, 10}", "Name", "Hall Number", "Capacity");
            foreach (Cinema i in CinemaList)
            {
                Console.WriteLine(i.Name, i.HallNo, i.Capacity);
            }
        }

        //loads Screenings
        static void Screening(List<Screening> ScreeningList, List<Movie> MovieList, List<Cinema> CinemaList)
        {
            using (StreamReader sr = new("Screening.csv"))
            {
                string r = sr.ReadLine();

                while ((r = sr.ReadLine()) != null)
                {
                    string[] x = r.Split(",");
                    Cinema cinema = CinemaList[0];
                    Movie movie = MovieList[0];
                    int ScreeningNo = Convert.ToInt32(x[0]);
                    DateTime ScreeningDateTime = Convert.ToDateTime(x[1]);
                    string ScreeningType = (x[2]);
                    foreach (Cinema c in CinemaList)
                    {
                        if (x[3] == c.Name)
                        {
                            cinema = c;

                        }
                    }
                    foreach (Movie m in MovieList)
                    {
                        if (x[4] == m.Title)
                        {
                            movie = m;

                        }
                    }


                    Screening s = new(ScreeningNo, ScreeningDateTime, ScreeningType, cinema, movie);
                    ScreeningList.Add(s);
                }
            }

            //Display Screenings
            static void DisplayScreening(List<Screening> ScreeningList)
            {
                Console.WriteLine("{0, 10} {1, 10} {2, 10} {3, 10} {4, 10} ", "Screening Number", "Date Time", "Format", "Cinema Name", "Movie Title");// format 
                foreach (Screening i in ScreeningList)
                {
                    Console.WriteLine(Convert.ToString(i.ScreeningNo), i.ScreeningDateTime, i.ScreeningType, i.CinemaName, i.MovieTitle);
                }
            }

            static void AddMovieSession(List<Movie> MovieList, List<Cinema> CinemaList, List<Screening> ScreeningList)
            {
                DisplayMovie(MovieList);
                Movie movie = MovieList[0];
                Cinema cinema = CinemaList[0];
                Console.WriteLine("Enter the movie title you want to watch: ");
                Movie title = Console.ReadLine();

                Console.WriteLine("Enter your screening type: ");
                String type = Console.ReadLine();
                while (type != "2D" || type != "3D")
                {

                    Console.WriteLine("Format does not exist");
                    Console.WriteLine("Enter your Screening type: ");
                }

                Console.WriteLine("Enter your screening time and date (e.g 04/02/2022 8:00PM): ");
                DateTime Screentime = Convert.ToDateTime(Console.ReadLine());

                DisplayCinema(CinemaList);

                Console.WriteLine("Enter the cinema name: ");
                Cinema cinema = Console.ReadLine();

                Console.WriteLine("Enter the Cinema hall no.: ");
                Screening halln = Console.ReadLine();

                Screening s = new(ScreeningList.Count + 1, Screentime, type, cinema, title);
            }

            static void ListMovieScreening()
            {
                Console.WriteLine("List of all the movies: ");
                Console.WriteLine("1) Phantom The Musical");
                Console.WriteLine("2) Makmum 2 ");
                Console.WriteLine("3) Special Delivery");
                Console.WriteLine("4) Nightmare Alley");
                Console.WriteLine("5) Scream");
                Console.WriteLine("6) Enna Solla Pogirai");
                Console.WriteLine("7) The Hating Game");
                Console.WriteLine("8) The Policeman's Lineage");
                Console.WriteLine("9) Sing 2");
                Console.WriteLine("10) Mulholland Drive");
                Console.WriteLine("Select the movie you want to watch: ");
                string choice = Console.ReadLine();
                if (choice == "Phantom The Musical")
                {
                    screeningptm();
                }
                if (choice == "Makmum 2")
                {
                    screeningm2();
                }
                if (choice == "Special Delivery")
                {
                    screeningsd();
                }
                if (choice == "Nightmare Alley")
                {
                    screeningna();
                }
                if (choice == "Scream")
                {
                    screeningscream();
                }
                if (choice == "Enna Solla Pogirai")
                {
                    screeningesp();
                }
                if (choice == "The Hating Game")
                {
                    screeningthg();
                }
                if (choice == "The Policeman's Lineage")
                {
                    screeningtpl();
                }
                if (choice == "Sing 2")
                {
                    screenings2();
                }
                if (choice == "Mulholland Drive")
                {
                    screeningmd();
                }
            }
            static void screeningptm()
            {
                string filePath = "Screening.csv";
                string[] lines = File.ReadAllLines(filePath);

                IEnumerable<string> selectLines = lines.Where(line => line.EndsWith("Phantom The Musical"));

                foreach (var item in selectLines)
                {
                    Console.WriteLine(item);
                }
            }

            static void screeningm2()
            {
                string filePath = "Screening.csv";
                string[] lines = File.ReadAllLines(filePath);

                IEnumerable<string> selectLines = lines.Where(line => line.EndsWith("Makmum 2"));

                foreach (var item in selectLines)
                {
                    Console.WriteLine(item);
                }
            }
            static void screeningsd()
            {
                string filePath = "Screening.csv";
                string[] lines = File.ReadAllLines(filePath);

                IEnumerable<string> selectLines = lines.Where(line => line.EndsWith("Special Delivery"));

                foreach (var item in selectLines)
                {
                    Console.WriteLine(item);
                }
            }
            static void screeningna()
            {
                string filePath = "Screening.csv";
                string[] lines = File.ReadAllLines(filePath);

                IEnumerable<string> selectLines = lines.Where(line => line.EndsWith("Nightmare Alley"));

                foreach (var item in selectLines)
                {
                    Console.WriteLine(item);
                }
            }
            static void screeningscream()
            {
                string filePath = "Screening.csv";
                string[] lines = File.ReadAllLines(filePath);

                IEnumerable<string> selectLines = lines.Where(line => line.EndsWith("Scream"));

                foreach (var item in selectLines)
                {
                    Console.WriteLine(item);
                }
            }
            static void screeningesp()
            {
                string filePath = "Screening.csv";
                string[] lines = File.ReadAllLines(filePath);

                IEnumerable<string> selectLines = lines.Where(line => line.EndsWith("Enna Solla Pogirai"));

                foreach (var item in selectLines)
                {
                    Console.WriteLine(item);
                }
            }
            static void screeningthg()
            {
                string filePath = "Screening.csv";
                string[] lines = File.ReadAllLines(filePath);

                IEnumerable<string> selectLines = lines.Where(line => line.EndsWith("The Hating Game"));

                foreach (var item in selectLines)
                {
                    Console.WriteLine(item);
                }
            }
            static void screeningtpl()
            {
                string filePath = "Screening.csv";
                string[] lines = File.ReadAllLines(filePath);

                IEnumerable<string> selectLines = lines.Where(line => line.EndsWith("The Policeman's Lineage"));

                foreach (var item in selectLines)
                {
                    Console.WriteLine(item);
                }
            }
            static void screenings2()
            {
                string filePath = "Screening.csv";
                string[] lines = File.ReadAllLines(filePath);

                IEnumerable<string> selectLines = lines.Where(line => line.EndsWith("Sing 2"));

                foreach (var item in selectLines)
                {
                    Console.WriteLine(item);
                }
            }
            static void screeningmd()
            {
                string filePath = "Screening.csv";
                string[] lines = File.ReadAllLines(filePath);

                IEnumerable<string> selectLines = lines.Where(line => line.EndsWith("Mulholland Drive"));

                foreach (var item in selectLines)
                {
                    Console.WriteLine(item);
                }
            }

            static void DeleteMovieScreening (List<Screening> ScreeningList, List <Movie> MovieList)
            {
                DisplayScreening(ScreeningList);
                Console.WriteLine("Enter the Screening Session to be deleted: ");
                int delete = Convert.ToInt32(Console.ReadLine());
                foreach (Movie m in MovieList)
                {
                    for (int i = 0; i < m.ScreeningList.Count(); i++)
                    {
                        if(delete == m.ScreeningList[i].ScreeningNo)
                        {
                            m.ScreeningList.Remove(m.ScreeningList[i]);
                            Console.WriteLine("Successfully deleted");
                        }
                    }
                }

            }








        }
    }
}

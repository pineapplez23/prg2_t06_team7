﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg2_Assignment
{
    class Cinema
    {
        public string Name
        {
            get;
            set;
        }
        public int HallNo
        {
            get;
            set;
        }
        public int Capacity
        {
            get;
            set;
        }

        public Cinema() {}

        public Cinema(string nm, int hn, int cap) 
        {
            Name = nm;
            HallNo = hn;
            Capacity = cap;
        }

        public override string ToString()
        {
            return "Name: " + Name + "HallNo: " + HallNo + "Capacity: " + Capacity  + "    ";
        }
    }
}

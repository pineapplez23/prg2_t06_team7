﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg2_Assignment
{
    class Order
    {
        public int OrderNo { get; set; }
        public DateTime OrderDateTime { get; set; }
        public double Amount { get; set; }
        public string Status { get; set; }
        public List <ticket> TicketList { get; set; }


        public Order()
        {

        }

        public Order(int on, DateTime odt)
        {
            OrderNo = on;
            OrderDateTime = odt;
        }

        public void AddTicket()
        {

        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}

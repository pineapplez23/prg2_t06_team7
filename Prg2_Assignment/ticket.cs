﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg2_Assignment
{
    class ticket:Screening
    {
        public Screening screening
        {
            get;
            set;
        }

        public ticket() 
        {

        }

        public ticket(Screening s) 
        {
            screening = s;
        }

        public virtual double CalculatePrice() 
        {
            return 0;
        }

        public override string ToString()
        {
            return "Screening: " + screening ;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg2_Assignment
{
    class Screening
    {
        public int ScreeningNo { get; set; }
        public DateTime ScreeningDateTime { get; set; }
        public string ScreeningType { get; set; }
        public Cinema CinemaName { get; set; }
        public Movie MovieTitle { get; set; }


        public Screening() {}

        public Screening(int no, DateTime date, string type, Cinema cinema, Movie title)
        {
            ScreeningNo = no;
            ScreeningDateTime = date;
            ScreeningType = type;
            CinemaName = cinema;
            MovieTitle= title;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}

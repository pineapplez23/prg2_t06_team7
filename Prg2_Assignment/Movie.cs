﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg2_Assignment
{
    class Movie
    {

        public string Title { get; set; }
        public int Duration { get; set; }
        public string Classification { get; set; }
        public DateTime OpeningDate { get; set; }
        public List<string> GenreList { get; set; } = new();
        public List<Screening> ScreeningList { get; set; } = new();


        public Movie() {}

        public Movie(string t, int d, string c, DateTime od, List <string> genre)
        {
            Title = t;
            Duration = d;
            Classification = c;
            OpeningDate = od;
            GenreList = genre;
        }

        public void AddGenre ( string genre)
        {
            GenreList.Add(genre);
        }

        public void AddScreening(Screening screening)
        {
            ScreeningList.Add(screening);
        }



        public override string ToString()
        {
            return base.ToString();
        }
    }
}

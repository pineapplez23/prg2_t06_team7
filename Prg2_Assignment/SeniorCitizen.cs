﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg2_Assignment
{
    class SeniorCitizen:ticket
    {
        public int YearOfBirth
        {
            get;
            set;
        }

        public SeniorCitizen()
        {

        }

        public SeniorCitizen(Screening s, int yob) : base(s)
        {
            YearOfBirth = yob;
        }

        public override double CalculatePrice()
        {
            return 0;
        }
        public override string ToString()
        {
            return "Year of birth: " + YearOfBirth;
        }
    }
}

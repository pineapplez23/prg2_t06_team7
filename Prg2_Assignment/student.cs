﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg2_Assignment
{
    class student:ticket
    {
        public string LevelOfStudy
        {
            get;
            set;
        }

        public student() 
        {
           
        }

        public student(Screening s,string los):base(s)
        {
            LevelOfStudy = los;
        }

        public override double CalculatePrice()
        {
            return 0;
        }
        public override string ToString()
        {
            return "Level of study: " + LevelOfStudy;
        }
    }
}

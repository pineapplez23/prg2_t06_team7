﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg2_Assignment
{
    class adult:ticket
    {
        public bool PopCornOffer
        {
            get;
            set;
        }

        public adult(){}

        public adult(Screening s, bool po) : base(s)
        {
            PopCornOffer = po;
        }

        public override double CalculatePrice()
        {
            return 0;
        }
        public override string ToString()
        {
            return "Popcorn offer: " + PopCornOffer;
        }
    }
}
